# YipYipSwift

[![CI Status](http://img.shields.io/travis/Marcel Bloemendaal/YipYipSwift.svg?style=flat)](https://travis-ci.org/Marcel Bloemendaal/YipYipSwift)
[![Version](https://img.shields.io/cocoapods/v/YipYipSwift.svg?style=flat)](http://cocoapods.org/pods/YipYipSwift)
[![License](https://img.shields.io/cocoapods/l/YipYipSwift.svg?style=flat)](http://cocoapods.org/pods/YipYipSwift)
[![Platform](https://img.shields.io/cocoapods/p/YipYipSwift.svg?style=flat)](http://cocoapods.org/pods/YipYipSwift)

## Usage

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

YipYipSwift is available through [CocoaPods](http://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod "YipYipSwift"
```

## Author

Marcel Bloemendaal, m.bloemendaal@yipyip.nl

## License

YipYipSwift is available under the MIT license. See the LICENSE file for more info.

## Changelog

* 0.1.26 - Added YipYipNibView, CHanged M_PI to CGFloat.pi