//
//  YipYip.swift
//  Pods
//
//  Created by Marcel Bloemendaal on 15/04/16.
//
//

import Foundation

open class YipYipUtils
{
    open static let view = ViewUtils()
    open static let files = FileUtils()
    open static let json = JSONUtils()
    open static let time = TimeUtils()
}
